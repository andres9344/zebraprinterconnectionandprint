package com.zebra.isv.util;

/**
 * Created by andresrodriguez on 8/24/16.
 */
public class DemoSleeper {

    private DemoSleeper() {

    }

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
